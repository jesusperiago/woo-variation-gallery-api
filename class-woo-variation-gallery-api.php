<?php

/**
 * Clase WooVariationGalleryApi.
 *
 * Esta clase administra la actualización de las imágenes de variación de productos
 * a través de una ruta de la API REST personalizada en WordPress.
 */
class WooVariationGalleryApi
{
    /**
     * Permisos necesarios para realizar la actualización.
     * @var string
     */
    public $permissions = "edit_posts";

    /**
     * Ruta del espacio de nombres para la API REST.
     * @var string
     */
    public $namespace_url = "wc-variation-custom/v1";

    /**
     * Ruta de la API REST para actualizar imágenes de variación de productos.
     * @var string
     */
    public $route = '/products/(?P<id_producto>\d+)/variations/(?P<id_variacion>\d+)';

    /**
     * Constructor de la clase. Registra la ruta de la API REST.
     */
    public function __construct()
    {
        add_action('rest_api_init', array($this, 'register_rest_route'));
    }

    /**
     * Registra la ruta de la API REST para actualizar las imágenes de variación del producto.
     */
    public function register_rest_route()
    {
        register_rest_route($this->namespace_url, $this->route, array(
            'methods' => 'PUT',
            'callback' => array($this, 'update_product_meta_variation'),
            'permission_callback' => array($this, 'update_product_meta_permissions_check'),
        ));
    }

    /**
     * Verifica los permisos del usuario para actualizar las imágenes de variación del producto.
     * @return bool True si el usuario tiene permisos adecuados, de lo contrario, False.
     */
    public function update_product_meta_permissions_check()
    {
        // Aquí puedes verificar si el usuario tiene permisos adecuados
        return current_user_can($this->permissions);
    }

    /**
     * Actualiza las imágenes de variación del producto.
     * @param array $data Datos de la solicitud REST.
     * @return WP_Error|WP_REST_Response WP_Error si hay errores, WP_REST_Response si es exitoso.
     */
    public function update_product_meta_variation($data)
    {
        $product_id = $data['id_producto'];
        $variation_id = $data['id_variacion'];
        $meta = $data['woo_variation_gallery_images'];

        $product = $this->getProductVariable($product_id);
        if (!$product) return new WP_Error('invalid_product', 'The product is invalid or not variable', array('status' => 400));

        $variation = $this->getVariationProduct($product, $variation_id);
        if (!$variation) return new WP_Error('no_variation', 'Variation not found', array('status' => 404));

        $gallery_image_ids = $this->getVariationsImages($meta);
        if (!$gallery_image_ids) return new WP_Error('bad_requests', 'Bad requests', array('status' => 400));

        $this->save_product_variation(array_unique($gallery_image_ids), $variation_id);
        return new WP_REST_Response($variation->get_data(), 200);
    }

    /**
     * Obtiene el objeto del producto.
     * @param int $product_id ID del producto.
     * @return object|bool Producto o False si no es válido.
     */
    public function getProductVariable($product_id)
    {
        $product = wc_get_product($product_id);

        // Si $product es null o no es un producto variable, retorna false
        if (!$product || !$product->is_type('variable')) {
            return false;
        }
        return $product;
    }

    /**
     * Obtiene la variación del producto.
     * @param object $product Producto variable.
     * @param int $variation_id ID de la variación.
     * @return object|bool Variación o False si no existe.
     */
    public function getVariationProduct($product, $variation_id)
    {
        // Obtiene la variación
        $variation = $product->get_child($variation_id);
        if (!$variation) {
            // Devolver un error si la variación no existe.
            return false;
        }
        return $variation;
    }

    /**
     * Obtiene y valida las imágenes de variación.
     * @param array $meta Datos de imágenes de variación.
     * @return array|bool Arreglo de IDs de imágenes o False si son inválidas.
     */
    public function getVariationsImages($meta)
    {
        if (gettype($meta) !== "array") return false;

        $gallery_image_ids = array_map('absint', $meta);

        foreach ($gallery_image_ids as $id) {
            // Obtiene el MIME Type del adjunto
            $mime_type = get_post_mime_type($id);
            // Verifica si el MIME Type es de una imagen
            if (!$mime_type || strpos($mime_type, 'image/') !== 0) {
                return false;
            }
        }
        return $gallery_image_ids;
    }

    /**
     * Guarda las imágenes de variación del producto en la base de datos.
     * @param array $gallery_image_ids Arreglo de IDs de imágenes.
     * @param int $variation_id ID de la variación del producto.
     */
    public function save_product_variation($gallery_image_ids, $variation_id)
    {
        update_post_meta($variation_id, 'woo_variation_gallery_images', $gallery_image_ids);
    }
}