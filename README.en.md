# Woo Variation Gallery API

The WooVariationGalleryApi class is a tool to enable the update of product variation images in WordPress through a custom REST API route. 

## Prerequisites

Antes de utilizar esta clase, asegúrate de tener lo siguiente:

- WordPress installed and configured.
- WooCommerce installed and activated on your website.
- [Additional Variation Images Gallery for WooCommerce](https://wordpress.org/plugins/woo-variation-gallery/) plugin installed.


## Installation

1. Download `class-woo-variation-gallery-api.php` from this repository.
2. Place the `class-woo-variation-gallery-api.php` file in the appropriate location in your WordPress project.

## Usage

Follow these steps to use the `class-woo-variation-gallery-api.php` class in your project:

### 1. Include the class in your code

Include the class in your functions file or in any file within your WordPress theme or plugin:

```php
include_once('ruta/a/class-woo-variation-gallery-api.php');
```

### 2. Create an instance of the class

Create an instance of the WooVariationGalleryApi class in your code:

```php
$product_variation_image_updater = new WooVariationGalleryApi();
```

### 3. Automatic registration of the REST API route

The class will automatically register the custom REST API route that allows you to update product variation images in WooCommerce.


### 4. User permissions

Ensure that users with the appropriate permissions (defined in the `$permissions` property of the class) can access this custom REST API.

### 5. Update variation images

To update the images of a product's variation, make a PUT request to the following URL:

```
https://miwordpress.es/wp-json/wc-variation-custom/v1/products/{id_producto}/variations/{id_variacion}
```
Replace {product_id} and {variation_id} with the appropriate values in your request.

### 6. PUT request data

Make sure to include the necessary data in the PUT request, such as the variation images, so that the class can process the update correctly.
```json
{
    "woo_variation_gallery_images": [22477, 22476]
}
```

### 7. Handling responses
The class will handle the update and return an appropriate response, either an error or a success confirmation.

## Example

Including the class in WordPress:
```php
// Add this to the functions.php file
include_once('ruta/a/class-woo-variation-gallery-api.php');

// Create an instance of the class
$product_variation_image_updater = new WooVariationGalleryApi();

```

Make the API request to the following URL:
```
https://miwordpress.es/wp-json/wc-variation-custom/v1/products/25260/variations/25400
```
