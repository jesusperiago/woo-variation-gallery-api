# Woo Variation Gallery API

La clase `WooVariationGalleryApi` es una herramienta para poder realizar la actualización de las imágenes de variación de productos en WordPress a través de una ruta de la API REST personalizada. 

## Requisitos previos

Antes de utilizar esta clase, asegúrate de tener lo siguiente:

- WordPress instalado y configurado.
- WooCommerce instalado y activado en tu sitio web.
- Instalado el plugin [Additional Variation Images Gallery for WooCommerce](https://wordpress.org/plugins/woo-variation-gallery/)


## Instalación

1. Descarga el archivo `class-woo-variation-gallery-api.php` de este repositorio.
2. Coloca el archivo `class-woo-variation-gallery-api.php` en la ubicación adecuada en tu proyecto de WordPress.

## Uso

Sigue estos pasos para utilizar la clase `class-woo-variation-gallery-api.php` en tu proyecto:

### 1. Incluye la clase en tu código

Incluye la clase en tu archivo de funciones o en cualquier archivo de tu tema o plugin de WordPress:

```php
include_once('ruta/a/class-woo-variation-gallery-api.php');
```

### 2. Crea una instancia de la clase

Crea una instancia de la clase WooVariationGalleryApi en tu código:

```php
$product_variation_image_updater = new WooVariationGalleryApi();
```

### 3. Registro automático de la ruta de la API REST

La clase se encargará automáticamente de registrar la ruta de la API REST personalizada que te permite actualizar las imágenes de variación de productos en WooCommerce.


### 4. Permisos de usuario
Asegúrate de que los usuarios con los permisos adecuados (definidos en la propiedad $permissions de la clase) puedan acceder a esta API REST personalizada.

### 5. Actualización de imágenes de variación
Para actualizar las imágenes de variación de un producto, realiza una solicitud PUT a la siguiente URL:

```
https://miwordpress.es/wp-json/wc-variation-custom/v1/products/{id_producto}/variations/{id_variacion}
```
Reemplaza {id_producto} y {id_variacion} con los valores adecuados en tu solicitud.

### 6. Datos de la solicitud PUT
Asegúrate de incluir los datos necesarios en la solicitud PUT, como las imágenes de variación, para que la clase pueda procesar la actualización correctamente.
```json
{
    "woo_variation_gallery_images": [22477, 22476]
}
```

### 7. Manejo de respuestas
La clase manejará la actualización y devolverá una respuesta adecuada, ya sea un error o una confirmación de éxito.

## Ejemplo

Incluir la clase a wordpress
```php
// Añadir en el fichero functions.php 
include_once('ruta/a/class-woo-variation-gallery-api.php');

// Crea una instancia de la clase
$product_variation_image_updater = new WooVariationGalleryApi();
```

Realiza la solicitud con la api a la siguiente dirección:

```
https://miwordpress.es/wp-json/wc-variation-custom/v1/products/25260/variations/25400
```